Closure Benchmark
===================

A naive benchmark about closures amongst different programming language implementations.

Usage
===================

### bench_clean.sh
Delete everything in the ./temp folder.

### bench_version.sh
Print version of a programming language implementation. Pass no argument to print all versions

    ./bench_version.sh <imp name> <imp name> ...

### bench_compile.sh
Compile a program for the benchmark. Pass no argument to compile all programs.

    ./bench_compile.sh <prog name> <prog name> ...

### bench_run.sh
Run and time a program. Pass no argument to run all programs.

    ./bench_run.sh <prog name> <prog name> ...

You can also use the following options:  
`--l=X` or `--loop=X` : Set the size of the main loop to X.  
`--d=X` or `--deep_level=X` : Set the amount of nesting closures to X.  
`--r=X` or `--repeat=X`: Set how many times each program are run to X.

### bench_test.sh
Test a program (print the result). Pass no argument to test all programs.  
Usefull for checking if an implementation can handle a specific deep level.

    ./bench_test.sh <prog name> <prog name> ...

You can also use the following options:  
`--l=X` or `--loop=X` : Set the size of the main loop to X.  
`--d=X` or `--deep_level=X` : Set the amount of nesting closures to X.  

Why ?
===================

The reason of this benchmark is not to prove anything, but rather to spark conversation
and to have an excuse for toying with closures.