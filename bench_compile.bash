#!/bin/bash


function compile_c_clang 
{
    echo -e "C clang"
    clang -O3 -march=native ./source/p_c_clang.c -fblocks -lBlocksRuntime -o ./temp/p_c_clang.elf
}

function compile_c++_clang++ 
{
    echo -e "C++ clang++"
    clang++ -O3 -march=native -std=c++14 ./source/p_c++.cpp -o ./temp/p_c++_clang.elf
}

function compile_c++_g++ 
{    
    echo -e "C++ G++"
    g++ -O3 -march=native -std=c++14 ./source/p_c++.cpp -o ./temp/p_c++_g++.elf
}

function compile_d_dmd
{
    echo -e "D DMD"
    dmd -O ./source/p_d.d -of./temp/p_d_dmd.elf
}

function compile_d_gdc
{    
    echo -e "D GDC"
    gdc -O3 -march=native ./source/p_d.d -o ./temp/p_d_gdc.elf
}

function compile_go_go
{    
    echo -e "Go Go"
    go build -o ./temp/p_go_go.elf ./source/p_go.go
}

function compile_go_gccgo
{
    echo -e "Go gccgo"
    gccgo -O3 -march=native ./source/p_go.go -o ./temp/p_go_gccgo.elf
}

function compile_javascript_nodejs 
{
    echo -e "Javascript Node.js"
}

function compile_python_cpython
{
    echo -e "Python CPython"
    python -c 'import py_compile; py_compile.compile("./source/p_python.py", "./temp/p_python_cpython.pyo", optimize=2)'
}

function compile_ruby_mri 
{
    echo -e "Ruby MRI"
}

function compile_scheme_guile
{
    echo -e "Scheme Guile"
    guild compile ./source/p_scheme_guile.scm > /dev/null
}

function compile_scheme_chicken
{
    echo -e "Scheme Chicken"
    csc -O3 ./source/p_scheme_chicken.scm -o ./temp/p_scheme_chicken.elf > /dev/null
}

function compile_rust_rustc 
{
    echo -e "Rust rustc"
    rustc -O ./source/p_rust.rs -o ./temp/p_rust.elf
}

function compile_crystal 
{
    echo -e "Crystal"
    crystal build --release ./source/p_crystal.cr -o ./temp/p_crystal.elf
}

function compile_vala 
{
    echo -e "Vala"
    valac ./source/p_vala.vala -o ./temp/p_vala.elf
}

function compile_r
{
    echo -e "R"
}

function compile_ocaml
{
    echo -e "OCaml"
    cp ./source/p_ocaml.ml ./temp/p_ocaml.ml
    ocamlopt -inline 20 -nodynlink ./temp/p_ocaml.ml -o ./temp/p_ocaml.elf
}

function compile_ruby_jruby
{
    echo -e "Ruby JRuby"
    jrubyc ./source/p_ruby.rb -t ./temp/
}

function compile_fsharp_mono
{
    echo -e "F# Mono"
    fsharpc --nologo ./source/p_fsharp.fs -o ./temp/p_fsharp_mono.exe
}

function compile_java_openjdk
{
    echo -e "Java OpenJDK"
    javac ./source/p_java.java -d ./temp/
}

function compile_groovy
{
    echo -e "Groovy"
}

function compile_dart
{
    echo -e "Dart"
}

function compile_lua_lua
{
    echo -e "Lua lua"
}

function compile_lua_luajit
{
    echo -e "Lua LuaJIT"
}

function compile_squirrel
{
    echo -e "Squirrel"
    squirrel -o ./temp/p_squirrel.cnut -c ./source/p_squirrel.nut
}

function compile_qore
{
    echo -e "Qore"
}

function compile_nim
{
    echo -e "Nim"
    nim compile -d:release --verbosity:0 --hints:off --opt:speed -W:off -x:off --nimcache:./temp/ -o:./temp/p_nim.elf ./source/p_nim.nim 
}

function compile_neko
{
    echo -e "Neko"
    cp ./source/p_neko.neko ./temp/p_neko.neko
    nekoc ./temp/p_neko.neko
}

function compile_julia
{
    echo -e "Julia"
}

function compile_potion
{
    echo -e "Potion"
    cp ./source/p_potion.pn ./temp/p_potion.pn
    potion -c ./temp/p_potion.pn > /dev/null
}

function compile_python_pypy
{
    echo -e "Python PyPy"
    pypy3 -c 'import py_compile; py_compile.compile("./source/p_python.py", "./temp/p_python_pypy.pyo", optimize=2)'
}

function compile_gura
{
    echo -e "Gura"
}

function compile_javascript_spidermonkey
{
    echo -e "Javascript SpiderMonkey"
}

function compile_racket
{
    echo -e "Racket"
    racket -e '(require compiler/compile-file) (compile-file "./source/p_racket.rkt" "./temp/p_racket.zo")'
}

function compile_scheme_gambit
{
    echo -e "Scheme Gambit"
    gambitc -exe -o ./temp/p_scheme_gambit.elf ./source/p_scheme_gambit.scm
}

function compile_d_ldc
{
    echo -e "D LDC"
    ldc -O3 ./source/p_d.d -of=./temp/p_d_ldc.elf
}

function compile_scheme_bigloo
{
    echo -e "Scheme Bigloo"
    cp ./source/p_scheme_bigloo.scm ./temp/p_scheme_bigloo.scm
    bigloo -native -O6 -unsafe ./temp/p_scheme_bigloo.scm -o ./temp/p_scheme_bigloo.elf
}

function compile_nemerle_mono
{
    echo -e "Nemerle Mono"
    ncc --nologo -O ./source/p_nemerle.n -o ./temp/p_nemerle_mono.exe
}

function compile_golo
{
    echo -e "Golo"
    golo compile ./source/p_golo.golo --output ./temp/p_golo
}

function compile_scala
{
    echo -e "Scala"
    scalac -optimise ./source/p_scala.scala -d ./temp/
}

function compile_haskell_ghc
{
    echo -e "Haskell GHC"
    ghc -O3 ./source/p_haskell.hs -odir ./temp/ -hidir ./temp/ -o ./temp/p_haskell > /dev/null
}

function compile_commonlisp_sbcl
{
    echo -e "Common Lisp SBCL"
    sbcl --noinform --eval "(compile-file \"./source/p_commonlisp_sbcl.lisp\" :output-file \"../temp/p_commonlisp_sbcl\")" --eval "(quit)" > /dev/null
}

function compile_commonlisp_clisp
{
    echo -e "Common Lisp CLISP"
    clisp -c ./source/p_commonlisp_clisp.lisp -o ./temp/p_commonlisp_clisp > /dev/null
}

function print_help
{
    echo "Compile all programs if no option."
    echo "You can select a specific program with:"
    echo "   ./bench_compile.bash <program name>"
    echo
    echo "Available programs:"

    for K in "${!complist[@]}"; do
        echo "   $K"
    done | sort -f
}

declare -A complist=(
    ["c_clang"]=compile_c_clang
    ["c++_clang++"]=compile_c++_clang++
    ["c++_g++"]=compile_c++_g++
    ["d_dmd"]=compile_d_dmd
    ["d_gdc"]=compile_d_gdc
    ["go_go"]=compile_go_go
    ["go_gccgo"]=compile_go_gccgo
    ["javascript_nodejs"]=compile_javascript_nodejs
    ["python_cpython"]=compile_python_cpython
    ["ruby_mri"]=compile_ruby_mri
    ["scheme_guile"]=compile_scheme_guile
    ["scheme_chicken"]=compile_scheme_chicken
    ["rust_rustc"]=compile_rust_rustc
    ["crystal"]=compile_crystal
    ["vala"]=compile_vala
    ["r"]=compile_r
    ["ocaml"]=compile_ocaml
    ["ruby_jruby"]=compile_ruby_jruby
    ["f#_mono"]=compile_fsharp_mono
    ["java_openjdk"]=compile_java_openjdk
    ["groovy"]=compile_groovy
    ["dart"]=compile_dart
    ["lua_lua"]=compile_lua_lua
    ["lua_luajit"]=compile_lua_luajit
    ["squirrel"]=compile_squirrel
    ["qore"]=compile_qore
    ["nim"]=compile_nim
    ["neko"]=compile_neko
    ["julia"]=compile_julia
    ["potion"]=compile_potion
    ["python_pypy"]=compile_python_pypy
    ["gura"]=compile_gura
    ["javascript_spidermonkey"]=compile_javascript_spidermonkey
    ["racket"]=compile_racket
    ["scheme_gambit"]=compile_scheme_gambit
    ["d_ldc"]=compile_d_ldc
    ["scheme_bigloo"]=compile_scheme_bigloo
    ["nemerle_mono"]=compile_nemerle_mono
    ["golo"]=compile_golo
    ["scala"]=compile_scala
    ["haskell_ghc"]=compile_haskell_ghc
    ["commonlisp_sbcl"]=compile_commonlisp_sbcl
    ["commonlisp_clisp"]=compile_commonlisp_clisp
)


if [ $# = 0 ]; then
    echo "Compiling..."
    for K in "${complist[@]}"; do
        $K
    done
    echo "Done!"
elif [ $1 = "--help" ]; then
    print_help
else
    echo "Compiling..."
    for ARG in $@; do
        if [ ${complist[$ARG]} ]; then
            ${complist[$ARG]}
        else
            echo "ERROR: invalid option '$ARG'."
            echo "Try './bench_compile.bash --help' for more information."
            exit
        fi
    done
    echo "Done!"
fi


