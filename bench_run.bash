#!/bin/bash

# You can sort the output with sort -t ":" -k2n 

# loop:
#
# Set the amount of iterations.
# 
# Default value: 100000
#
loop=100000 

# deep_level:
#
# Set the amount of nested closures.
# Maximum for spidermonkey is 390.
# Otherwise, all other programs accept at least 1000.
# 
# Default value: 390
#
deep_level=390 

# repeat:
#
# Set how many times a program is executed.
# Try 'man perf-stat' for more information.
#
# Default value: 5
#
repeat=1

with_deviation="grep time"
without_deviation="grep -o ^.*elapsed"
grep_cmd=$without_deviation

function run_c_clang
{
	printf "%-25s" "C clang: "
	perf stat -r $repeat ./temp/p_c_clang.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_c++_clang++
{
	printf "%-25s" "C++ clang++: "
	perf stat -r $repeat ./temp/p_c++_clang.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_c++_g++
{
	printf "%-25s" "C++ G++: "
	perf stat -r $repeat ./temp/p_c++_g++.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_d_dmd
{
	printf "%-25s" "D DMD: "
	perf stat -r $repeat ./temp/p_d_dmd.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_d_gdc
{
	printf "%-25s" "D GDC: "
	perf stat -r $repeat ./temp/p_d_gdc.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_go_go
{
	printf "%-25s" "Go Go: "
	perf stat -r $repeat ./temp/p_go_go.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_go_gccgo
{
	printf "%-25s" "Go gccgo: "
	perf stat -r $repeat ./temp/p_go_gccgo.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_javascript_nodejs
{
	printf "%-25s" "Javascript node.js: "
	perf stat -r $repeat node ./source/p_javascript_nodejs.js $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_python_cpython
{
	printf "%-25s" "Python CPython: "
	perf stat -r $repeat python ./temp/p_python_cpython.pyo $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_ruby_mri
{
	printf "%-25s" "Ruby MRI: "
	perf stat -r $repeat ruby ./source/p_ruby.rb $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_scheme_guile
{
	printf "%-25s" "Scheme Guile: "
	perf stat -r $repeat guile ./source/p_scheme_guile.scm $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_scheme_chicken
{
	printf "%-25s" "Scheme Chicken: "
	perf stat -r $repeat ./temp/p_scheme_chicken.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_rust_rustc
{
	printf "%-25s" "Rust rustc: "
	perf stat -r $repeat ./temp/p_rust.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_crystal
{
	printf "%-25s" "Crystal: "
	perf stat -r $repeat ./temp/p_crystal.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_vala
{
	printf "%-25s" "Vala: "
	perf stat -r $repeat ./temp/p_vala.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_r
{
	printf "%-25s" "R: "
	perf stat -r $repeat R -q --slave -f ./source/p_r.R --args $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_ocaml
{
	printf "%-25s" "OCaml: "
	perf stat -r $repeat ./temp/p_ocaml.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_ruby_jruby
{
	printf "%-25s" "Ruby JRuby: "
	perf stat -r $repeat jruby ./temp/source/p_ruby.class $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_fsharp_mono
{
	printf "%-25s" "F# Mono: "
	perf stat -r $repeat mono ./temp/p_fsharp_mono.exe $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_java_openjdk
{
	printf "%-25s" "Java OpenJDK: "
	perf stat -r $repeat java -classpath ./temp/ p_java $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_groovy
{
	printf "%-25s" "Groovy: "
	export JAVA_OPTS="$JAVA_OPTS -Xss15m"
	perf stat -r $repeat groovy ./source/p_groovy $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_dart
{
	printf "%-25s" "Dart: "
	perf stat -r $repeat dart ./source/p_dart.dart $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_lua_lua
{
	printf "%-25s" "Lua Lua: "
	perf stat -r $repeat lua ./source/p_lua.lua $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_lua_luajit
{
	printf "%-25s" "Lua LuaJIT: "
	perf stat -r $repeat luajit -O3 ./source/p_lua.lua $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_squirrel
{
	printf "%-25s" "Squirrel: "
	perf stat -r $repeat squirrel ./temp/p_squirrel.cnut $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_qore
{
	printf "%-25s" "Qore: "
	perf stat -r $repeat qore ./source/p_qore.q $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_nim
{
	printf "%-25s" "Nim: "
	perf stat -r $repeat ./temp/p_nim.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_neko
{
	printf "%-25s" "Neko: "
	perf stat -r $repeat neko ./temp/p_neko.n $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_julia
{
	printf "%-25s" "Julia: "
	perf stat -r $repeat julia --check-bounds=no ./source/p_julia.jl $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_potion
{
	printf "%-25s" "Potion: "
	perf stat -r $repeat potion -X ./temp/p_potion.pnb $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_python_pypy
{
	printf "%-25s" "Python PyPy: "
	perf stat -r $repeat pypy3 ./temp/p_python_pypy.pyo $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_gura
{
	printf "%-25s" "Gura: "
	perf stat -r $repeat gura ./source/p_gura.gura $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_javascript_spidermonkey
{
	printf "%-25s" "Javascript SpiderMonkey: "
	perf stat -r $repeat js24 ./source/p_javascript_spidermonkey.js $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_racket
{
	printf "%-25s" "Racket: "
	perf stat -r $repeat racket ./temp/p_racket.zo $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_scheme_gambit
{
	printf "%-25s" "Scheme Gambit: "
	perf stat -r $repeat ./temp/p_scheme_gambit.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_d_ldc
{
	printf "%-25s" "D LDC: "
	perf stat -r $repeat ./temp/p_d_ldc.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_scheme_bigloo
{
	printf "%-25s" "Scheme Bigloo: "
	perf stat -r $repeat ./temp/p_scheme_bigloo.elf $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_nemerle_mono
{
    printf "%-25s" "Nemerle Mono: "
    perf stat -r $repeat mono ./temp/p_nemerle_mono.exe $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_golo
{
    printf "%-25s" "Golo: "
    perf stat -r $repeat golo run --classpath ./temp/p_golo/ --module p_golo $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_scala
{
    printf "%-25s" "Scala: "
    perf stat -r $repeat scala -classpath ./temp/ p_scala $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_haskell_ghc
{
    printf "%-25s" "Haskell GHC: "
    perf stat -r $repeat ./temp/p_haskell $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_commonlisp_sbcl
{
    printf "%-25s" "Common Lisp SBCL: "
    perf stat -r $repeat sbcl --noinform --load "./temp/p_commonlisp_sbcl.fasl" --quit --end-toplevel-options 0 $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function run_commonlisp_clisp
{
    printf "%-25s" "Common Lisp CLISP: "
    perf stat -r $repeat clisp ./temp/p_commonlisp_clisp.fas $loop $deep_level 2>&1 > /dev/null | $grep_cmd
}

function print_help
{
	echo "Run all programs if no option."
	echo "You can select a specific program with:"
	echo "   ./bench_run.bash <program name>"
	echo
	echo "Available programs:"

	for K in "${!runlist[@]}"; do
		echo "   $K"
	done | sort -f
}

declare -A runlist=(
	["c_clang"]=run_c_clang
	["c++_clang++"]=run_c++_clang++
	["c++_g++"]=run_c++_g++
	["d_dmd"]=run_d_dmd
	["d_gdc"]=run_d_gdc
	["go_go"]=run_go_go
	["go_gccgo"]=run_go_gccgo
	["javascript_nodejs"]=run_javascript_nodejs
	["python_cpython"]=run_python_cpython
	["ruby_mri"]=run_ruby_mri
	["scheme_guile"]=run_scheme_guile
	["scheme_chicken"]=run_scheme_chicken
	["rust_rustc"]=run_rust_rustc
	["crystal"]=run_crystal
	["vala"]=run_vala
	["r"]=run_r
	["ocaml"]=run_ocaml
	["ruby_jruby"]=run_ruby_jruby
	["f#_mono"]=run_fsharp_mono
	["java_openjdk"]=run_java_openjdk
	["groovy"]=run_groovy
	["dart"]=run_dart
	["lua_lua"]=run_lua_lua
	["lua_luajit"]=run_lua_luajit
	["squirrel"]=run_squirrel
	["qore"]=run_qore
	["nim"]=run_nim
	["neko"]=run_neko
	["julia"]=run_julia
	["potion"]=run_potion
	["python_pypy"]=run_python_pypy
	["gura"]=run_gura
	["javascript_spidermonkey"]=run_javascript_spidermonkey
	["racket"]=run_racket
	["scheme_gambit"]=run_scheme_gambit
	["d_ldc"]=run_d_ldc
	["scheme_bigloo"]=run_scheme_bigloo
	["nemerle_mono"]=run_nemerle_mono
	["golo"]=run_golo
	["scala"]=run_scala
	["haskell_ghc"]=run_haskell_ghc
	["commonlisp_sbcl"]=run_commonlisp_sbcl
	["commonlisp_clisp"]=run_commonlisp_clisp
)

# Parse the arguments list for any option.
# Found options are removed from the list.
declare -a argv=("$@")

for ARG in "${!argv[@]}"; do
	case ${argv[$ARG]} in
	-l=*|--loop=*)
		loop=${argv[$ARG]#*=}
		unset argv[$ARG]
		;;
	-d=*|--deep_level=*)
		deep_level=${argv[$ARG]#*=}
		unset argv[$ARG]
		;;
	-r=*|--repeat=*)
		repeat=${argv[$ARG]#*=}
		unset argv[$ARG]
		;;
	-h|--help)
		print_help
		exit
		;;
	esac
done

# If the now filtered arguments list is empty, all programs are run.
# Otherwise, only the passed ones are.
if [ ${#argv[@]} -eq 0 ]; then
	for E in "${runlist[@]}"; do
		$E
	done
else
	for ARG in ${argv[@]}; do
		if [ ${runlist[$ARG]} ]; then
			${runlist[$ARG]}
		else
			echo "ERROR: invalid option '$ARG'."
			echo "Try './bench_run.bash --help' for more information."
			exit
		fi
	done
fi
