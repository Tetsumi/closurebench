#!/bin/bash

loop=2 
deep_level=390 

function test_c_clang
{
    printf "%-25s" "C clang: "
    ./temp/p_c_clang.elf $loop $deep_level
}

function test_c++_clang++
{
    printf "%-25s" "C++ clang++: "
    ./temp/p_c++_clang.elf $loop $deep_level
}

function test_c++_g++
{
    printf "%-25s" "C++ G++: "
    ./temp/p_c++_g++.elf $loop $deep_level
}

function test_d_dmd
{
    printf "%-25s" "D DMD: "
    ./temp/p_d_dmd.elf $loop $deep_level
}

function test_d_gdc
{
    printf "%-25s" "D GDC: "
    ./temp/p_d_gdc.elf $loop $deep_level
}

function test_go_go
{
    printf "%-25s" "Go Go: "
    ./temp/p_go_go.elf $loop $deep_level
}

function test_go_gccgo
{
    printf "%-25s" "Go gccgo: "
    ./temp/p_go_gccgo.elf $loop $deep_level
}

function test_javascript_nodejs
{
    printf "%-25s" "Javascript node.js: "
    node ./source/p_javascript_nodejs.js $loop $deep_level
}

function test_python_cpython
{
    printf "%-25s" "Python CPython: "
    python ./temp/p_python_cpython.pyo $loop $deep_level
}

function test_ruby_mri
{
    printf "%-25s" "Ruby MRI: "
    ruby ./source/p_ruby.rb $loop $deep_level
}

function test_scheme_guile
{
    printf "%-25s" "Scheme Guile: "
    guile ./source/p_scheme_guile.scm $loop $deep_level
}

function test_scheme_chicken
{
    printf "%-25s" "Scheme Chicken: "
    ./temp/p_scheme_chicken.elf $loop $deep_level
}

function test_rust_rustc
{
    printf "%-25s" "Rust rustc: "
    ./temp/p_rust.elf $loop $deep_level
}

function test_crystal
{
    printf "%-25s" "Crystal: "
    ./temp/p_crystal.elf $loop $deep_level
}

function test_vala
{
    printf "%-25s" "Vala: "
    ./temp/p_vala.elf $loop $deep_level
}

function test_r
{
    printf "%-25s" "R: "
    R -q --slave -f ./source/p_r.R --args $loop $deep_level | grep -o "[[:digit:]]*$"
}

function test_ocaml
{
    printf "%-25s" "OCaml: "
    ./temp/p_ocaml.elf $loop $deep_level
}

function test_ruby_jruby
{
    printf "%-25s" "Ruby JRuby: "
    jruby ./temp/source/p_ruby.class $loop $deep_level
}

function test_fsharp_mono
{
    printf "%-25s" "F# Mono: "
    mono ./temp/p_fsharp_mono.exe $loop $deep_level
}

function test_java_openjdk
{
    printf "%-25s" "Java OpenJDK: "
    java -classpath ./temp/ p_java $loop $deep_level
}

function test_groovy
{
    printf "%-25s" "Groovy: "
    export JAVA_OPTS="$JAVA_OPTS -Xss15m"
    groovy ./source/p_groovy $loop $deep_level
}

function test_dart
{
    printf "%-25s" "Dart: "
    dart ./source/p_dart.dart $loop $deep_level
}

function test_lua_lua
{
    printf "%-25s" "Lua Lua: "
    lua ./source/p_lua.lua $loop $deep_level
}

function test_lua_luajit
{
    printf "%-25s" "Lua LuaJIT: "
    luajit -O3 ./source/p_lua.lua $loop $deep_level
}

function test_squirrel
{
    printf "%-25s" "Squirrel: "
    squirrel ./temp/p_squirrel.cnut $loop $deep_level
}

function test_qore
{
    printf "%-25s" "Qore: "
    qore ./source/p_qore.q $loop $deep_level
}

function test_nim
{
    printf "%-25s" "Nim: "
    ./temp/p_nim.elf $loop $deep_level
}

function test_neko
{
    printf "%-25s" "Neko: "
    neko ./temp/p_neko.n $loop $deep_level
}

function test_julia
{
    printf "%-25s" "Julia: "
    julia --check-bounds=no ./source/p_julia.jl $loop $deep_level
}

function test_potion
{
    printf "%-25s" "Potion: "
    potion -X ./temp/p_potion.pnb $loop $deep_level
}

function test_python_pypy
{
    printf "%-25s" "Python PyPy: "
    pypy3 ./temp/p_python_pypy.pyo $loop $deep_level
}

function test_gura
{
    printf "%-25s" "Gura: "
    gura ./source/p_gura.gura $loop $deep_level
}

function test_javascript_spidermonkey
{
    printf "%-25s" "Javascript SpiderMonkey: "
    js24 ./source/p_javascript_spidermonkey.js $loop $deep_level
}

function test_racket
{
    printf "%-25s" "Racket: "
    racket ./temp/p_racket.zo $loop $deep_level
}

function test_scheme_gambit
{
    printf "%-25s" "Scheme Gambit: "
    ./temp/p_scheme_gambit.elf $loop $deep_level
}

function test_d_ldc
{
    printf "%-25s" "D LDC: "
    ./temp/p_d_ldc.elf $loop $deep_level
}

function test_scheme_bigloo
{
    printf "%-25s" "Scheme Bigloo: "
    ./temp/p_scheme_bigloo.elf $loop $deep_level
}

function test_nemerle_mono
{
    printf "%-25s" "Nemerle Mono: "
    mono ./temp/p_nemerle_mono.exe $loop $deep_level
}

function test_golo
{
    printf "%-25s" "Golo: "
    golo run --classpath ./temp/p_golo/ --module p_golo $loop $deep_level
}

function test_scala
{
    printf "%-25s" "Scala: "
    scala -classpath ./temp/ p_scala $loop $deep_level
}

function test_haskell_ghc
{
    printf "%-25s" "Haskell GHC: "
    ./temp/p_haskell $loop $deep_level
}

function test_commonlisp_sbcl
{
    printf "%-25s" "Common Lisp SBCL: "
    sbcl --noinform --load "./temp/p_commonlisp_sbcl.fasl" --quit --end-toplevel-options 0 $loop $deep_level
}

function test_commonlisp_clisp
{
    printf "%-25s" "Common Lisp CLISP: "
    clisp ./temp/p_commonlisp_clisp.fas $loop $deep_level
}

function print_help
{
    echo "Test all programs if no option."
    echo "You can select a specific program with:"
    echo "   ./bench_run.bash <program name>"
    echo
    echo "Available programs:"

    for K in "${!runlist[@]}"; do
        echo "   $K"
    done | sort -f
}

declare -A runlist=(
    ["c_clang"]=test_c_clang
    ["c++_clang++"]=test_c++_clang++
    ["c++_g++"]=test_c++_g++
    ["d_dmd"]=test_d_dmd
    ["d_gdc"]=test_d_gdc
    ["go_go"]=test_go_go
    ["go_gccgo"]=test_go_gccgo
    ["javascript_nodejs"]=test_javascript_nodejs
    ["python_cpython"]=test_python_cpython
    ["ruby_mri"]=test_ruby_mri
    ["scheme_guile"]=test_scheme_guile
    ["scheme_chicken"]=test_scheme_chicken
    ["rust_rustc"]=test_rust_rustc
    ["crystal"]=test_crystal
    ["vala"]=test_vala
    ["r"]=test_r
    ["ocaml"]=test_ocaml
    ["ruby_jruby"]=test_ruby_jruby
    ["f#_mono"]=test_fsharp_mono
    ["java_openjdk"]=test_java_openjdk
    ["groovy"]=test_groovy
    ["dart"]=test_dart
    ["lua_lua"]=test_lua_lua
    ["lua_luajit"]=test_lua_luajit
    ["squirrel"]=test_squirrel
    ["qore"]=test_qore
    ["nim"]=test_nim
    ["neko"]=test_neko
    ["julia"]=test_julia
    ["potion"]=test_potion
    ["python_pypy"]=test_python_pypy
    ["gura"]=test_gura
    ["javascript_spidermonkey"]=test_javascript_spidermonkey
    ["racket"]=test_racket
    ["scheme_gambit"]=test_scheme_gambit
    ["d_ldc"]=test_d_ldc
    ["scheme_bigloo"]=test_scheme_bigloo
    ["nemerle_mono"]=test_nemerle_mono
    ["golo"]=test_golo
    ["scala"]=test_scala
    ["haskell_ghc"]=test_haskell_ghc
    ["commonlisp_sbcl"]=test_commonlisp_sbcl
    ["commonlisp_clisp"]=test_commonlisp_clisp
)

# Parse the arguments list for any option.
# Found options are removed from the list.
declare -a argv=("$@")

for ARG in "${!argv[@]}"; do
    case ${argv[$ARG]} in
    -l=*|--loop=*)
        loop=${argv[$ARG]#*=}
        unset argv[$ARG]
        ;;
    -d=*|--deep_level=*)
        deep_level=${argv[$ARG]#*=}
        unset argv[$ARG]
        ;;
    -h|--help)
        print_help
        exit
        ;;
    esac
done

# If the now filtered arguments list is empty,
# all programs are run. Otherwise, only the passed ones are.
if [ ${#argv[@]} -eq 0 ]; then
    for E in "${runlist[@]}"; do
        $E
    done
else
    for ARG in ${argv[@]}; do
        if [ ${runlist[$ARG]} ]; then
            ${runlist[$ARG]}
        else
            echo "ERROR: invalid option '$ARG'."
            echo "Try './bench_test.bash --help' for more information."
            exit
        fi
    done
fi
