#!/bin/bash

function version_mono 
{
	printf "%-20s" "Mono: "
	mono --version | grep Mono
}

function version_openjdk
{
	printf "%-20s" "OpenJDK: "
	java -version 2>&1 | grep Server
}

function version_gcc
{
	printf "%-20s" "GCC: "
	gcc --version | grep gcc
}

function version_clang
{	
	printf "%-20s" "Clang: "
	clang --version | grep version
}

function version_clang++
{	
	printf "%-20s" "Clang++: "
	clang++ --version | grep version
}

function version_g++
{	
	printf "%-20s" "G++: "
	g++ --version | grep g++
}

function version_dmd
{	
	printf "%-20s" "DMD: "
	dmd --version | grep Compiler
}

function version_gdc
{	
	printf "%-20s" "GDC: "
	gdc --version | grep gdc
}

function version_go
{	
	printf "%-20s" "Go: "
	go version
}

function version_gccgo
{	
	printf "%-20s" "Gccgo: "
	gccgo --version | grep gccgo
}

function version_nodejs
{	
	printf "%-20s" "Node.js: "
	node --version
}

function version_cpython
{	
	printf "%-20s" "CPython: "
	python --version
}

function version_rubymri
{	
	printf "%-20s" "Ruby MRI: "
	ruby --version
}

function version_guile
{	
	printf "%-20s" "Guile: "
	guile --version | grep guile
}

function version_chicken
{	
	printf "%-20s" "Chicken Scheme: "
	csc -version | grep Version
}

function version_rustc
{	
	printf "%-20s" "Rustc: "
	rustc --version
}

function version_crystal	
{
	printf "%-20s" "Crystal: "
	crystal --version
}

function version_vala
{
	printf "%-20s" "Vala: "
	vala --version
}

function version_r
{
	printf "%-20s" "R: "
	R --version | grep "R version"
}

function version_ocaml
{	
	printf "%-20s" "OCaml: "
	ocamlopt -v | grep version
}

function version_jruby
{	
	printf "%-20s" "JRuby: "
	jruby --version
}

function version_fsharp
{	
	printf "%-20s" "F#: "
	fsharpc --help | grep Compiler
}

function version_groovy
{	
	printf "%-20s" "Groovy: "
	groovy --version
}

function version_dart
{	
	printf "%-20s" "Dart: "
	dart --version 2>&1
}

function version_lua
{	
	printf "%-20s" "Lua: "
	lua -v
}

function version_luajit
{	
	printf "%-20s" "LuaJIT: "
	luajit -v
}

function version_squirrel
{	
	printf "%-20s" "Squirrel: "
	squirrel -v
}

function version_qore
{	
	printf "%-20s" "Qore: "
	qore -V | grep version
}

function version_nim
{	
	printf "%-20s" "Nim: "
	nim --version 2>&1 | grep Nim
}

function version_neko
{	
	printf "%-20s" "Neko: "
	nekoc -help | grep Compiler
}

function version_julia
{	
	printf "%-20s" "Julia: "
	julia --version
}

function version_potion
{	
	printf "%-20s" "Potion: "
	potion --version
}

function version_pypy
{	
	printf "%-20s" "PyPy: "
	pypy3 --version 2>&1 | grep PyPy
}

function version_gura
{	
	printf "%-20s" "Gura: "
	gura -v 2>&1
}

function version_spidermonkey
{
	printf "%-20s" "SpiderMonkey: "
	js24 -h | grep -o 'JavaScript-.*'
}

function version_racket
{
	printf "%-20s" "Racket: "
	racket --version
}

function version_gambit
{
	printf "%-20s" "Gambit: "
	gsi -e '(println (system-version-string) " " (system-stamp))'
}

function version_ldc
{
    printf "%-20s" "LDC: "
    ldc --version | grep compiler
}

function version_bigloo
{
    printf "%-20s" "Bigloo: "
    bigloo -version
}

function version_nemerle
{
    printf "%-20s" "Nemerle: "
    ncc -version 2>&1 | grep Compiler
}

function version_golo
{
    printf "%-20s" "Golo: "
    golo version --full | grep Golo
}

function version_scala
{
    printf "%-20s" "Scala: "
    scala -version  
}

function version_ghc
{
    printf "%-20s" "GHC: "
    ghc --version
}

function version_sbcl
{
    printf "%-20s" "SBCL: "
    sbcl --version
}

function version_clisp
{
    printf "%-20s" "CLISP: "
    clisp --version | grep "GNU CLISP"
}

function print_help
{
	echo "Print version of all implementations if no option."
	echo "You can select a specific implementaion with:"
	echo "   ./bench_version.bash <implementation name>"
	echo
	echo "Available implementations:"

	for K in "${!implist[@]}"; do
		echo "   $K"
	done | sort -f
}

declare -A implist=(
	["mono"]=version_mono 
	["openjdk"]=version_openjdk
	["gcc"]=version_gcc
	["clang"]=version_clang
	["clang++"]=version_clang++
	["g++"]=version_g++
	["dmd"]=version_dmd
	["gdc"]=version_gdc
	["go"]=version_go
	["gccgo"]=version_gccgo
	["node.js"]=version_nodejs
	["cpython"]=version_cpython
	["rubymri"]=version_rubymri
	["guile"]=version_guile
	["chicken"]=version_chicken
	["rustc"]=version_rustc
	["crystal"]=version_crystal
	["vala"]=version_vala
	["r"]=version_r
	["ocaml"]=version_ocaml
	["jruby"]=version_jruby
	["f#"]=version_fsharp
	["groovy"]=version_groovy
	["dart"]=version_dart
	["lua"]=version_lua
	["luajit"]=version_luajit
	["squirrel"]=version_squirrel
	["qore"]=version_qore
	["nim"]=version_nim
	["neko"]=version_neko
	["julia"]=version_julia
	["potion"]=version_potion
	["pypy"]=version_pypy
	["gura"]=version_gura
	["spidermonkey"]=version_spidermonkey
	["racket"]=version_racket
	["gambit"]=version_gambit
	["ldc"]=version_ldc
	["bigloo"]=version_bigloo
	["nemerle"]=version_nemerle
	["golo"]=version_golo
	["scala"]=version_scala
	["ghc"]=version_ghc
	["sbcl"]=version_sbcl
	["clisp"]=version_clisp
)

if [ $# = 0 ]; then
	for E in "${implist[@]}"; do
		$E
	done | sort -f
elif [ $1 = "--help" ]; then
	print_help
else
	for ARG in $@; do
		if [ ${implist[$ARG]} ]; then
			${implist[$ARG]}
		else
			echo "ERROR: invalid option '$ARG'."
			echo "Try './bench_version.bash --help' for more information."
			exit
		fi
	done | sort -f
fi


