/*
    Tetsumi <tetsumi@vmail.me>

    9 April 2015
*/

#include <iostream>
#include <functional>

std::function<int()> make_closures (const int n, const int x)
{
    if (0 >= n)
        return [=] ()->int { return x; };

    return [l = make_closures(n - 1, x), x] ()->int { return l() + 1 + x; };
}

int main (int argc, const char* argv[])
{
    using namespace std;

    if (3 > argc) {
        cout << "Error bad arguments: Need 2 integers."
             << endl;
        
        return 0;
    }

    int counter = atoi(argv[1]);
    const int deep_level = atoi(argv[2]);
    int result = 0;
        
    while (0 <= --counter) {
        const auto f_a = make_closures(deep_level,  counter);
        const auto f_b = make_closures(deep_level, -counter);
        
        result += f_a();
        result += f_b();
    }

    cout << result << endl;
}