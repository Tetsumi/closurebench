/*
    Tetsumi <tetsumi@vmail.me>

    9 April 2015
*/

#include <stdio.h>
#include <stdlib.h>
#include <Block.h>

typedef int (^ClosureInt)();

ClosureInt make_closures(const int n, const int x)
{
    if (0 >= n)
        return Block_copy(^ int { return x; });
    
    const ClosureInt l = make_closures(n - 1, x);
    const ClosureInt ret = Block_copy(^ int { return l() + 1 + x; });
    
    Block_release(l);
    
    return ret;
}

int main (int argc, char *argv[])
{
    if (3 > argc) {
        puts("Error bad arguments: Need 2 integers.");
        
        return 0;
    }

    int counter = atoi(argv[1]);
    const int deep_level = atoi(argv[2]);
    int result = 0;
    
    
    while (0 <= --counter) {
        ClosureInt f_a = make_closures(deep_level,  counter);
        ClosureInt f_b = make_closures(deep_level, -counter);
        
        result += f_a();
        result += f_b();
        
        Block_release(f_a);
        Block_release(f_b);
    }
    
    printf("%d\n", result); 
}