;;
;; Anonymous
;; Tetsumi <tetsumi@vmail.me>
;;
;; 14 August 2015
;;

(defparameter *result* 0)

(defun make-closures (n x)
    (if (>= 0 n)
        (lambda () x)
        (flet ((l () (make-closures (- n 1) x)))
          (lambda () (+ (funcall (l)) 1 x)))))

(defun main-program (counter depth)
  (loop for count from counter downto 1 do
    (flet ((f_a () (make-closures depth count))
           (f_b () (make-closures depth (- count))))
        (setf *result* (+ *result* (funcall (f_a))))
        (setf *result* (+ *result* (funcall (f_b)))))))
  

(defun main ()
  (if (< (length ext:*args*) 2)
      (format t "Error: bad arguments, need two integers~%")
      (progn
        (main-program
         (parse-integer (car ext:*args*))
         (parse-integer (cadr ext:*args*)))
        (format t "~a~%" *result*))))

(main)
