#
# Tetsumi <tetsumi@vmail.me>
#
# 9 April 2015
#

def make_closures (n, x)
    if 0 >= n
        return ->() { x }
    end
    
    l = make_closures(n - 1, x)
    return ->() { l.call() + 1 + x }
end

if ARGV.size < 2
    puts "Error bad arguments: Need 2 integers."
    exit
end

counter = ARGV[0].to_i
deep_level =  ARGV[1].to_i
result = 0

while counter > 0
    counter -= 1
    
    f_a = make_closures(deep_level,  counter)
    f_b = make_closures(deep_level, -counter)
    
    result += f_a.call()
    result += f_b.call()
end

puts result