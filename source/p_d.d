/*
    Tetsumi <tetsumi@vmail.me>

    9 April 2015
*/

import std.stdio;
import std.conv;

alias int delegate() Closure; 

Closure make_closures (const int n, const int x)
{
    if (0 >= n) {
        int cl () { return x; }

        return &cl;
    }
    
    const auto l = make_closures(n - 1, x);
    
    int cl2 () { return l() + 1 + x; }
    
    return &cl2;
}

void main (in string[] argv)
{
    if (3 > argv.length) {
        writeln("Error bad arguments: Need 2 integers.");
        
        return;
    }

    int counter = to!int(argv[1]);
    const int deep_level = to!int(argv[2]);
    int result = 0;
    
    while (0 <= --counter) {
        const auto f_a = make_closures(deep_level,  counter);
        const auto f_b = make_closures(deep_level, -counter);

        result += f_a();
        result += f_b();
    }

    writeln(result);
}