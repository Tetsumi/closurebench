/*
    Tetsumi <tetsumi@vmail.me>
    
    16 April 2015
*/

make_closures(final int n, final int x)
{
    if (0 >= n)
        return () => x;
        
    var l = make_closures(n - 1, x);
    
    return () => l() + 1 + x;
}

void main(List<String> args)
{
    if (2 > args.length) {
        print("Error bad arguments: Need 2 integers.");
        
        return;
    }

    int counter = int.parse(args[0]);
    final int deep_level = int.parse(args[1]);
    int result = 0;
    
    while (0 <= --counter) {
        var f_a = make_closures(deep_level,  counter);
        var f_b = make_closures(deep_level, -counter);
        
        result += f_a();
        result += f_b();
    }
    
    print(result);  
}