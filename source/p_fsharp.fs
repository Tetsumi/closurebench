//
//       Tetsumi <tetsumi@vmail.me>
//        
//       15 April 2015
//

let rec make_closures(n, x) =
    if 0 >= n then
        fun () -> x
    else
        let l = make_closures(n - 1, x);
        fun () -> l() + 1 + x;;

[<EntryPoint>]
let main(args) =
    if 2 > args.Length then
        printfn "Error bad arguments: Need 2 integers.";
        0
    else
        let counter = ref(int(args.[0]))
        let deep_level = int(args.[1])
        let result = ref 0
        
        while 0 < !counter do
            counter := !counter - 1;

            let f_a = make_closures(deep_level,   !counter);
            let f_b = make_closures(deep_level, -(!counter));
                
            result := !result + f_a();
            result := !result + f_b();
                
        printfn "%d" !result
        0;;