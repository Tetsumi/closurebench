/*
    Tetsumi <tetsumi@vmail.me>

    9 April 2015
*/

package main

import "fmt"
import "os"
import "strconv"


func make_closures(n int, x int) func() int {
    if 0 >= n {
        return func() int {
            return x;
        }
    }
    
    l := make_closures(n - 1, x)
    
    return func() int {
        return l() + 1 + x
    }
}

func main()  {
    if 3 > len(os.Args) {
        fmt.Println("Error bad arguments: Need 2 integers.")

        return
    }

    counter, _ := strconv.Atoi(os.Args[1])
    deep_level, _ := strconv.Atoi(os.Args[2])
    result := 0

    
    for 0 < counter {
        counter--
        f_a := make_closures(deep_level,  counter)
        f_b := make_closures(deep_level, -counter)
        
        result += f_a()
        result += f_b() 
    }
    
    fmt.Println(result)
}