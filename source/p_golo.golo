#
#   Tetsumi <tetsumi@vmail.me>
#
#   28 April 2015
#

module p_golo

local function make_closures = |n, x| {
    if 0 >= n {
        return { return x }
    }
    
    let l = make_closures(n - 1, x)
    
    return { return l() + 1 + x }
}

function main = |args| {
    if 2 > args: length() {
        println("Error bad arguments: Need 2 integers.")
        System.exit(0)
    }
    
    var counter = intValue(args: get(0))
    let deep_level = intValue(args: get(1))
    var result = 0
    
    while 0 < counter {
        counter = counter - 1
        
        let f_a = make_closures(deep_level,     counter)
        let f_b = make_closures(deep_level, 0 - counter)
        
        result = result + f_a()
        result = result + f_b()
    }
    
    println(result)
}