/*
    Tetsumi <tetsumi@vmail.me>
    
    16 April 2015
*/

class l_groovy
{

    static make_closures(final n, final x)
    {
        if (0 >= n)
            return { x }
            
        final l = make_closures(n - 1, x)
        
        return {l() + 1 + x}
    }
    
    static void main(String[] args)
    {
        if (2 > args.length) {
            println "Error bad arguments: Need 2 integers."
            
            return
        }
        
        int counter = args[0].toInteger()
        final int deep_level = args[1].toInteger()
        int result = 0
        
        while (0 <= --counter) {
            final f_a = make_closures(deep_level,  counter)
            final f_b = make_closures(deep_level, -counter)
            
            result += f_a()
            result += f_b()
        }
        
        println result
    }
}