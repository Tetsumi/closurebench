--
-- Anonymous
--
-- 14 August 2015
--

import System.Environment(getArgs)

makeClosures :: Int -> Int -> a -> Int
makeClosures n x = if 0 >= n then const x
                   else let l = makeClosures (n - 1) x in
                     \_ -> l() +  1 + x

mainLoop :: Int -> Int -> Int -> IO()
mainLoop 0 _ result = print result
mainLoop counter depth result =
  mainLoop (counter - 1) depth (result + 
                                makeClosures depth counter () + 
                                makeClosures depth (-counter) ())

main :: IO()
main = do
  args <- getArgs
  if length args < 2 then
    putStrLn "Error: bad arguments, need two integers"
  else
    case args of
      (counter : depth : _) -> mainLoop (read counter) (read depth) 0 
