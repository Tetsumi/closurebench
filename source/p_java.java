/*
    Tetsumi <tetsumi@vmail.me>
    
    15 April 2015
*/

import java.util.function.IntSupplier;

class p_java
{
    static IntSupplier make_closures(final int n, final int x)
    {
        if (0 >= n)
            return () -> { return x; };

        final IntSupplier l = make_closures(n - 1, x);
        
        return () -> { return l.getAsInt() + 1 + x; };
    }
    
    public static void main (String args[])
    {
        if (2 > args.length) {
            System.out.println("Error bad arguments: Need 2 integers.");
            
            return;
        }
        
        int counter = Integer.parseInt(args[0]);
        final int deep_level = Integer.parseInt(args[1]);
        int result = 0;
        
        while (0 <= --counter) {
            final IntSupplier f_a = make_closures(deep_level,  counter);
            final IntSupplier f_b = make_closures(deep_level, -counter);
            
            result += f_a.getAsInt();
            result += f_b.getAsInt();
        }
        
        System.out.println(result);
    }
}