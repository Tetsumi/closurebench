/*
    Tetsumi <tetsumi@vmail.me>
    
    9 April 2015
*/

function make_closures (n, x)
{
    if ( 0 >= n)
        return function(x) { 
            return function() { return x; } 
        }(x)

    l = make_closures(n - 1, x);
    
    return function(l) { 
        return function() { return l() + 1 + x; }
    }(l)
}

if (4 > process.argv.length) {
    console.log("Error bad arguments: Need 2 integers.");
    process.exit(0);
}

counter = Number(process.argv[2])
deep_level = Number(process.argv[3])
result = 0

while (0 <= --counter) {
    f_a = make_closures(deep_level,  counter);
    f_b = make_closures(deep_level, -counter);
    
    result += f_a();
    result += f_b();
}

console.log(result);
