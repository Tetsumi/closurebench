/*
    Tetsumi <tetsumi@vmail.me>
    
    9 April 2015
*/

function make_closures (n, x)
{
    if ( 0 >= n)
        return function(x) { 
            return function() { return x; } 
        }(x)

    l = make_closures(n - 1, x);
    
    return function(l) { 
        return function() { return l() + 1 + x; }
    }(l)
}

if (2 > scriptArgs.length) {
    print("Error bad arguments: Need 2 integers.");
    quit();
}

counter = Number(scriptArgs[0])
deep_level = Number(scriptArgs[1])
result = 0

while (0 <= --counter) {
    f_a = make_closures(deep_level,  counter);
    f_b = make_closures(deep_level, -counter);
    
    result += f_a();
    result += f_b();
}

print(result);
