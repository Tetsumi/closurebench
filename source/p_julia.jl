#=
    Tetsumi <tetsumi@vmail.me>
    
    16 April 2015
=#

function make_closures (n::Int, x::Int)
    if 0 >= n
        return () -> x
    end

    l = make_closures(n - 1, x)
    
    return () -> l() + 1 + x
end

if 2 > length(ARGS)
    println("Error bad arguments: Need 2 integers.")
    quit()
end

counter = int(ARGS[1])
deep_level = int(ARGS[2])
result = 0
    
while 0 < counter
    counter -= 1

    local f_a = make_closures(deep_level,  counter)
    local f_b = make_closures(deep_level, -counter)
    
    result += f_a()
    result += f_b()
end

println(result)