--
-- Tetsumi <tetsumi@vmail.me>
--
-- 16 April 2015
--

function make_closures (n, x)
    if 0 >= n then
        return function () 
            return x 
        end
    end
    
    local l = make_closures(n - 1, x)
    
    return function () 
        return l() + 1 + x 
    end 
end

counter = tonumber(arg[1])
deep_level = tonumber(arg[2])

if counter == nil or deep_level == nil then
    print("Error bad arguments: Need 2 integers.")
    os.exit(0)
end

result = 0

while 0 < counter do
    counter = counter - 1
    
    local f_a = make_closures(deep_level,  counter)
    local f_b = make_closures(deep_level, -counter)
    
    result = result + f_a()
    result = result + f_b()
end

print(result)