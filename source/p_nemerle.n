/*
        Tetsumi <tetsumi@vmail.me>
        
        26 April 2015
*/


using System;

def make_closures (n, x)
{
    if (0 >= n)
        fun () { x };
    else {
        def l = make_closures (n - 1, x);
    
        fun () { l () + 1 + x };
    }
}

def argv = Environment.GetCommandLineArgs ();

when (3 > argv.Length) {
    Console.WriteLine ("Error bad arguments: Need 2 integers.");
    Environment.Exit(0);
}
    
mutable counter = int.Parse (argv[1]);
def deep_level = int.Parse (argv[2]);
mutable result = 0;

while (0 < counter) {
    --counter;
    def f_a = make_closures (deep_level,  counter);
    def f_b = make_closures (deep_level, -counter);
    
    result += f_a ();
    result += f_b ();
}

Console.WriteLine (result);