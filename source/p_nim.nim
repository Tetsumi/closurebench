#
#   Tetsumi <tetsumi@vmail.me>
#
#   16 April 2015
#

import os
import strutils

proc make_closures (n: int, x: int): proc():int =
    if 0 >= n:
        return proc ():int = x
    
    let l = make_closures(n - 1, x)
    
    return proc ():int = l() + 1 + x     

let argv = commandLineParams()

if 2 > len(argv):
    echo "Error bad arguments: Need 2 integers."
    quit()

var counter = parseInt(argv[0])
let deep_level = parseInt(argv[1])
var result = 0

while 0 < counter:
    counter -= 1
    
    let f_a = make_closures(deep_level,  counter)
    let f_b = make_closures(deep_level, -counter)
    
    result += f_a()
    result += f_b()
       
echo result