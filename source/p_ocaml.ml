(*
    Tetsumi <tetsumi@vmail.me>
    
    15 April 2015   
*)

open Printf

let rec make_closures (n, x) =
    if 0 >= n then
        function() -> x
    else
        let l = make_closures(n - 1, x) in
            function() -> l() + 1 + x

let () =
    if 3 > Array.length Sys.argv then begin
        printf "Error bad arguments: Need 2 integers.\n"
    end else begin
        let counter = ref(int_of_string(Sys.argv.(1))) in
        let deep_level = int_of_string(Sys.argv.(2)) in
        let result = ref 0 in
            while 0 < !counter do
                counter := !counter - 1;

                let f_a = make_closures(deep_level,   !counter) in
                let f_b = make_closures(deep_level, -(!counter)) in
                    result := !result + f_a();
                    result := !result + f_b();
            done;       
            printf "%d\n" !result
    end;
    


