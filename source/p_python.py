#
# Tetsumi <tetsumi@vmail.me>
#
# 9 April 2015
#

import sys

sys.setrecursionlimit(1500)

def make_closures (n, x):
    if 0 >= n:
        def cl():
            return x
        return cl
    
    l = make_closures (n - 1, x)
    
    def cl():
        return l() + 1 + x
    
    return cl

if len(sys.argv) < 3:
    print("Error bad arguments: Need 2 integers.")
    sys.exit(0)
    
counter = int(sys.argv[1])
deep_level =  int(sys.argv[2])
result = 0

while counter > 0:
    counter -= 1
    
    f_a = make_closures(deep_level,  counter)
    f_b = make_closures(deep_level, -counter)
    
    result += f_a()
    result += f_b()

print(result)