#
#   Tetsumi <tetsumi@vmail.me>
#
#   16 April 2015
#

closure sub make_closures (int $n, int $x)
{
    if (0 >= $n)
        return sub() {return $x;};
    
    my $l = make_closures($n - 1, $x);
    
    return sub() {return $l() + 1 + $x;};
}

if (2 > $ARGV.size()) {
    printf("Error bad arguments: Need 2 integers.\n");
    exit(0);
}

my int $counter = int($ARGV[0]);
my int $deep_level = int($ARGV[1]);
my int $result = 0;

while (0 <= --$counter) {
    my closure $f_a = make_closures($deep_level,  $counter);
    my closure $f_b = make_closures($deep_level, -$counter);
    
    $result += $f_a();
    $result += $f_b();
}

printf("%d\n", $result);