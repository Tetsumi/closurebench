/*
    Tetsumi <tetsumi@vmail.me>

    9 April 2015
*/

use std::env;

fn make_closures (n:i32, x:i32) -> Box<Fn() -> i32>
{
    if 0 >= n {
        return Box::new(move || x);
    }

    let l = make_closures(n - 1, x);
    
    return Box::new(move || l() + 1 + x);
}

fn main()
{
    let argv: Vec<String> = env::args().collect();

    if 3 > argv.len() {
        println!("Error bad arguments: Need 2 integers.");
        
        return;
    }

    let mut counter = argv[1].parse::<i32>().unwrap();
    let deep_level = argv[2].parse::<i32>().unwrap();
    let mut result = 0;
    
    while 0 < counter {
        counter -= 1;
        
        let f_a = make_closures(deep_level,  counter);
        let f_b = make_closures(deep_level, -counter);
        
        result += f_a();
        result += f_b();        
    }
    
    println!("{}", result);
}