/*
    Tetsumi <tetsumi@vmail.me>
    
    9 May 2015
*/

object p_scala 
{
    def make_closures(n:Int, x:Int): () => Int =
    {
        if (0 >= n) {
            return () => x
        }
        
        val l = make_closures(n - 1, x)
        
        return () => l() + 1 + x
    }

    def main(args: Array[String])
    {
        if (2 > args.length) {
            println("Error bad arguments: Need 2 integers.")
            return
        }
        
        var counter = args(0).toInt
        val deep_level = args(1).toInt
        var result = 0
        
         while (0 < counter) {
            counter -= 1
            
            val f_a = make_closures(deep_level,  counter)
            val f_b = make_closures(deep_level, -counter)
            
            result += f_a()
            result += f_b()
        }
        
        println(result)
    }
}
