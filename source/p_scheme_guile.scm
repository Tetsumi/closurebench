;
; Tetsumi <tetsumi@vmail.me>
;
; 9 April 2015
;

(define (make_closures n x)
    (if (>= 0 n)
        (lambda () x)
        (let ((l (make_closures (- n 1) x)))
             (lambda () (+ (l) 1 x)))))
            


(define argv (command-line))

(when (> 3 (length argv))
    (begin 
        (display "Error bad arguments: Need 2 integers.")
        (newline)
        (exit)))

(define counter (string->number (car (cdr argv))))
(define deep_level (string->number (car (cdr (cdr argv)))))
(define result 0)

(define (loopfunc)
    (when (< 0 counter)
        (begin (set! counter (- counter 1))
        (let ((f_a (make_closures deep_level    counter))
              (f_b (make_closures deep_level (- counter))))
             (set! result (+ result (f_a)))
             (set! result (+ result (f_b)))
             (loopfunc)))))
    
(loopfunc)
(display result)
(newline)