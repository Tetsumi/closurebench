/*
    Tetsumi <tetsumi@vmail.me>
    
    16 April 2015
*/

function make_closures (n, x)
{
    if (0 >= n)
        return @() x
        
    local l = make_closures(n - 1, x)
    
    return @() l() + 1 + x
}

if (2 > vargv.len()) {
    print("Error bad arguments: Need 2 integers.\n")

    return
}

local counter = vargv[0].tointeger()
local deep_level = vargv[1].tointeger()
local result = 0

while (0 <= --counter) {
    local f_a = make_closures(deep_level,  counter)
    local f_b = make_closures(deep_level, -counter)
    
    result += f_a()
    result += f_b()
}

print(format("%d\n", result))

