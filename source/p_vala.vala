/*
    Tetsumi <tetsumi@vmail.me>

    9 April 2015
*/

delegate int Closure();


static Closure make_closures (int n, int x) 
{
    if (0 >= n) {
        return () => x;
    }
        
    Closure l = make_closures(n - 1, x);
    
    return () => l() + 1 + x;
}



static int main (string[] argv)
{
    if (3 > argv.length) {
        stdout.printf("Error bad arguments: Need 2 integers.");

        return 0;
    }
    
    int counter = int.parse(argv[1]);
    int deep_level = int.parse(argv[2]);
    int result = 0;

    while (0 <= --counter) {
        Closure f_a = make_closures(deep_level,  counter);
        Closure f_b = make_closures(deep_level, -counter);
        
        result += f_a();
        result += f_b();
    }
    
    stdout.printf("%d\n", result);

    return 0;
}